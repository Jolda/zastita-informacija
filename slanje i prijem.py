from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
message = b'Polozio sam vezbe!'
print("SENDING:"+str(message))
key = RSA.importKey(open('public.pem').read())
cipher = PKCS1_OAEP.new(key)
ciphertext = cipher.encrypt(message)
print("CRYPTING:"+str(ciphertext))
key = RSA.importKey(open('private.pem').read())
cipher = PKCS1_OAEP.new(key)
message = cipher.decrypt(ciphertext)
print("RECIVING:"+str(message))
